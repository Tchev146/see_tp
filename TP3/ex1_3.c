#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#define NB_THREADS	3

typedef struct
{
  double value;
} thread_param;

void *travailUtile(void * args)
{
   int i;
   double resultat=0.0;
   thread_param * param = (thread_param *) args;

   for (i=0; i<1000000; i++)
   {
     resultat = resultat + param->value;
   }
   printf("resultat = %e\n",resultat);
   free(param);
   pthread_exit((void *) 0);
}

int main 
(
   int argc, 
   char ** argv
)
{
   pthread_t thread[NB_THREADS];
   pthread_attr_t attr;
   thread_param * param;
   int rc, t;
   void *status;

   size_t stack_size = 0;

   /* Initialisation et activation d’attributs */
   pthread_attr_init(&attr); //valeur par défaut
   pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE); //attente du thread possible

   /* Get Stack size */
   pthread_attr_getstacksize(&attr, &stack_size);

   printf("Taille de la stack par defaut : %ld\n", stack_size);

   /* Set stack size */ 
   pthread_attr_setstacksize(&attr, stack_size+1000000);

   /* Get new Stack size */
   pthread_attr_getstacksize(&attr, &stack_size);

   printf("Taille de la stack : %ld\n", stack_size);

   for(t=0; t<NB_THREADS; t++)
   {
      printf("Creation du thread %d\n", t);
      param = (thread_param *) malloc(sizeof(thread_param));
      param->value = random();

      rc = pthread_create(&thread[t], &attr, travailUtile, param); 
      if (rc)
      {
         printf("ERROR; le code de retour de pthread_create() est %d\n", rc);
         exit(-1);
      }
   }

   /* liberation des attributs et attente de la terminaison des threads */
   pthread_attr_destroy(&attr);
   for(t=0; t<NB_THREADS; t++)
   {
      rc = pthread_join(thread[t], &status);
      if (rc)
      {
         printf("ERROR; le code de retour du pthread_join() est %d\n", rc);
         exit(-1);
      }
      printf("le join a fini avec le thread %d et a donne le status= %ld\n",t, (long)status);
   }

   pthread_exit(NULL);
}



