#define _GNU_SOURCE

/****************************************
 * INCLUDE ******************************
 ****************************************/
#include <aio.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <pthread.h>
#include <mqueue.h>

/***************************************
 * DEFINE ******************************
 ***************************************/
#define NB_THREADS 2
#define VECTOR_SIZE 400

/***************************************
 * STRUCT ****************************** 
 ***************************************/
typedef struct
{
  int index_start;
  int index_end;
  int vector;
} thread_param;

typedef struct
{
  int * vecteur1;
  int * vecteur2;
  int * vecteur3;
  int * vecteur4;
  int resultat;
  int compteur_affichage;
} produit;

/***************************************
 * GLOBAL VARIABLE *********************
 ***************************************/
produit prod;
mqd_t fd_file;
mqd_t code;

/***************************************
 * FUNCTION ****************************
 ***************************************/
void init_io 
(
  struct aiocb * io,
  FILE * fd,
  int offset,
  int reqprio,
  int nbytes,
  int buff_size
)
{
  io->aio_buf = malloc(buff_size);
  if (io->aio_buf == NULL)
  {
    perror("malloc io->aio_buf :");
    exit(EXIT_FAILURE);
  }

  io->aio_fildes = fileno(fd); //récupérer le descripteur d’un fichier à partir de son nom
  io->aio_nbytes = nbytes;
  io->aio_offset = offset;
  io->aio_reqprio = reqprio;

}

void read_io 
(
  struct aiocb * io
)
{
  __ssize_t retour_code = 0;
  retour_code = aio_read(io);
  if (retour_code == -1)
  {
    perror("aio_read() : ");
    exit(EXIT_FAILURE);
  }
}

void return_io 
(
  struct aiocb * io,
  int lecture
)
{
  __ssize_t retour_code = 0;
  retour_code = aio_return(io);
  if (retour_code == -1)
  {
    perror("aio_return : ");
    exit(EXIT_FAILURE);
  }
  printf("operation AIO %d a retouree %d\n", lecture, retour_code);
  printf("io.aio_buf %d : %p\n", lecture, io->aio_buf);
}

void * threadAffichage
(
  void * args
)
{
  char * res_thread = NULL;
  struct mq_attr attr;
  mq_getattr(fd_file, &attr);

  res_thread = (char *) malloc(sizeof(char)*attr.mq_msgsize);

  while(1)
  {
    code = mq_receive(fd_file, res_thread, attr.mq_msgsize, 0);
    if (code == -1)
    {
      perror("mq_receive error : ");
      pthread_exit((void *) 0);
    }

    prod.resultat += atoi(res_thread);
    prod.compteur_affichage++;
    if (prod.compteur_affichage == NB_THREADS)
      break;
  }

  if(prod.compteur_affichage == NB_THREADS)
    printf("Resultat = %d\n", prod.resultat);

  free(res_thread);
}

void * produitScalaire
(
  void * args
)
{
  thread_param * param = (thread_param *) args;
  int i;
  int res_local = 0;
  char * buff;

  if (param->vector == 1)
  {
    for (i = param->index_start; i < param->index_end; i++)
    {
      res_local += prod.vecteur1[i] * prod.vecteur3[i];
    }
  }
  else if (param->vector == 2) 
  {
    for (i = param->index_start; i < param->index_end; i++)
    {
      res_local += prod.vecteur2[i] * prod.vecteur4[i];
    }
  }

  buff = (char *) malloc(sizeof(char)*sizeof(int));

  sprintf(buff, "%d", res_local);

  code = mq_send(fd_file, buff, sizeof(buff), 0);
  if (code == -1)
  {
    perror("mq_send error : ");
    pthread_exit((void *) 0);
  }

  free(param);

  pthread_exit((void *) 0);
}

/***************************************
 * MAIN ********************************
 ***************************************/

int main
(
  int argc, 
  char ** argv
)
{
  if(argc != 3)
  {
    printf("Usage: %s file1 file2\n", argv[0]);
    return (EXIT_FAILURE);
  }

  FILE * fd_vector1;
  FILE * fd_vector2;

  struct aiocb read1, read2, read3, read4;

  thread_param * param;

  __ssize_t retour_code = 0;

  pthread_t thread[NB_THREADS+1];
  pthread_attr_t attr;

  void *status;

  int compteur = 0;

  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

  struct aiocb * cbs[4];

  prod.compteur_affichage = 0;
  prod.resultat = 0;
  prod.vecteur1 = (int *) malloc(sizeof(int)*VECTOR_SIZE/2);
  prod.vecteur2 = (int *) malloc(sizeof(int)*VECTOR_SIZE/2);
  prod.vecteur3 = (int *) malloc(sizeof(int)*VECTOR_SIZE/2);
  prod.vecteur4 = (int *) malloc(sizeof(int)*VECTOR_SIZE/2);

  printf("Ouverture fichier1\n");

  fd_vector1 = fopen(argv[1], "r");
  if (fd_vector1 == NULL)
  {
    perror("fopen file1 :");
    exit(EXIT_FAILURE);
  }

  printf("Ouverture fichier2\n");
  fd_vector2 = fopen(argv[2], "r");
  if (fd_vector2 == NULL)
  {
    perror("fopen file2 :");
    exit(EXIT_FAILURE);
  }

  int nbytes = sizeof(int)*(VECTOR_SIZE/2);

  printf("Initialisation des lectures aio\n");
  init_io(&read1, fd_vector1, 0, 0, nbytes, nbytes);
  init_io(&read2, fd_vector1, nbytes, 0, nbytes, nbytes);
  init_io(&read3, fd_vector2, 0, 0, nbytes, nbytes);
  init_io(&read4, fd_vector2, nbytes, 0, nbytes, nbytes);

  printf("Lancement des requetes aio\n");
  //lancer la lecture
  read_io(&read1);
  read_io(&read2);
  read_io(&read3);
  read_io(&read4);

  //Suspension du processus dans l’attente de la terminaison de la lecture
  cbs[0] = &read1;
  cbs[1] = &read2;
  cbs[2] = &read3;
  cbs[3] = &read4;

  int request = 4;
  
  printf("Attente de la fin des requetes \n");
	while (request > 0)
  {
		for (int i = 0; i < 4; i++)
    {
			if (cbs[i] != NULL)
      {
        retour_code = aio_error(cbs[i]);
				if (retour_code == 0)
        {
					cbs[i] = NULL;
					request--;
				}
			}
		}
	}

  printf("Affichage du retour des requetes \n");
  return_io(&read1, 1);
  return_io(&read2, 2);
  return_io(&read3, 3);
  return_io(&read4, 4);

  fd_file = mq_open("/file", O_RDWR | O_CREAT, 0664, NULL);
  if (fd_file == -1)
  {
    perror("mq_open error : ");
    exit(EXIT_FAILURE);
  }

  for (int i = 0; i < VECTOR_SIZE/2; i++)
  {
    memcpy((int *) prod.vecteur1, (int *) read1.aio_buf, sizeof(int)*(VECTOR_SIZE/2));
    memcpy((int *) prod.vecteur2, (int *) read2.aio_buf, sizeof(int)*(VECTOR_SIZE/2));
    memcpy((int *) prod.vecteur3, (int *) read3.aio_buf, sizeof(int)*(VECTOR_SIZE/2));
    memcpy((int *) prod.vecteur4, (int *) read4.aio_buf, sizeof(int)*(VECTOR_SIZE/2));
  }

  for (compteur = 0; compteur<NB_THREADS; compteur++)
  {
    printf("Creation du thread %d\n", compteur);

    param = (thread_param *) malloc(sizeof(thread_param));

    param->index_end = VECTOR_SIZE/2;
    param->index_start = 0;
    param->vector = compteur+1;

    retour_code = pthread_create(&thread[compteur], &attr, produitScalaire, param);
    if (retour_code)
    {
      printf("ERROR; le code de retour de pthread_create() est %d\n", retour_code);
      exit(EXIT_FAILURE);
    }
  }
  
  printf("Creation du thread %d : thread affichage\n", NB_THREADS);

  retour_code = pthread_create(&thread[NB_THREADS], &attr, threadAffichage, NULL);  
  if (retour_code)
  {
    printf("ERROR; le code de retour de pthread_create() est %d\n", retour_code);
    exit(EXIT_FAILURE);
  }

  /* liberation des attributs et attente de la terminaison des threads */
  pthread_attr_destroy(&attr);
  for(compteur=0; compteur<NB_THREADS; compteur++)
  {
    retour_code = pthread_join(thread[compteur], &status);
    if (retour_code)
    {
      printf("ERROR; le code de retour du pthread_join() est %d\n", retour_code);
      exit(EXIT_FAILURE);
    }
    printf("le join a fini avec le thread %d et a donne le status= %ld\n",compteur, (long)status);
  }

  retour_code = pthread_join(thread[NB_THREADS], &status);
  if (retour_code)
  {
    printf("ERROR; le code de retour du pthread_join() est %d\n", retour_code);
    exit(EXIT_FAILURE);
  }
  printf("le join a fini avec le thread %d et a donne le status= %ld\n",NB_THREADS, (long)status);

  pthread_exit(NULL);

  free((void *) read1.aio_buf);
  free((void *) read2.aio_buf);
  free((void *) read3.aio_buf);
  free((void *) read4.aio_buf);
  free(prod.vecteur1);
  free(prod.vecteur2);
  free(prod.vecteur3);
  free(prod.vecteur4);


  fclose(fd_vector1);
  fclose(fd_vector2);

  // Destruction de la file de messages 

  code = mq_unlink("/file");
  if (code != 0 )
  {
    printf("ERROR; le code de retour du mq_unlink() est %d\n", code);
    exit(EXIT_FAILURE);
  }

  // Ferme le descripteur de fichier
  code = mq_close(fd_file);
  if (code != 0 )
  {
    printf("ERROR; le code de retour du mq_close() est %d\n", code);
    exit(EXIT_FAILURE);
  }

  return(EXIT_SUCCESS);
}
