#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * trim
(
  char * buff
)
{
  //PRECEDING CHARACTERS
  while (1==1)
  {
    if ((*buff == ' ') || (*buff == '\t') || (*buff == '\r') || (*buff == '\n'))
    { 
      ++buff;
    }
    else
      break;
  }
    
  //TRAILING CHARACTERS
  int y = strlen(buff)-1;
  while (1==1)
  {
    if (buff[y] == ' ' || (buff[y] == '\t') || (buff[y] == '\r') || (buff[y] == '\n'))
    { 
      y--;
    }
    else
      break;
  }
  y = strlen(buff)-y;
  buff[strlen(buff)-y+1]='\0';
  return buff;
}

void parse_cpu
(
  char * buffer
)
{
  char delim[2] = ":";
  char * token = NULL;
  char * tok_no_space = NULL;

  int ret_strcmp = 0;

  token = strtok(buffer, delim);
  tok_no_space = trim(token); 

  ret_strcmp = strcmp(tok_no_space, "model name");
  if (ret_strcmp == 0)
  {
    token = strtok(NULL, delim);
    tok_no_space = trim(token);
    printf("\tModel : %s\n", tok_no_space);
  }

  ret_strcmp = strcmp(tok_no_space, "cpu MHz");
  if (ret_strcmp == 0)
  {
    token = strtok(NULL, delim);
    tok_no_space = trim(token);
    printf("\tFrequency : %s\n", tok_no_space);
  }

  ret_strcmp = strcmp(tok_no_space, "cache size");
  if (ret_strcmp == 0)
  {
    token = strtok(NULL, delim);
    tok_no_space = trim(token);
    printf("\tCache : %s\n", tok_no_space);
  }

  ret_strcmp = strcmp(tok_no_space, "address sizes");
  if (ret_strcmp == 0)
  {
    token = strtok(NULL, delim);
    tok_no_space = trim(token);
    printf("\tEspace d'adressage : %s\n\n", tok_no_space);
  }

}

void parse_mem
(
  char * buffer
)
{
  char delim[2] = ":";
  char * token = NULL;
  char * tok_no_space = NULL;

  int ret_strcmp = 0;

  token = strtok(buffer, delim);
  tok_no_space = trim(token);

  ret_strcmp = strcmp(tok_no_space, "MemTotal");
  if (ret_strcmp == 0)
  {
    token = strtok(NULL, delim);
    tok_no_space = trim(token);
    printf("\tTotal memory : %s\n", tok_no_space);
  }

  ret_strcmp = strcmp(tok_no_space, "MemFree");
  if (ret_strcmp == 0)
  {
    token = strtok(NULL, delim);
    tok_no_space = trim(token);
    printf("\tFree memory : %s\n", tok_no_space);
  }
}

void parse_disk
(
  char * buffer
)
{

  char * part = NULL;
  int major = 0;
  int minor = 0;
  int blocks_size = 0;
  int blocks_size_mo = 0;

  if ((strstr(buffer, "sd")) != NULL)
  {
    part = (char*) malloc(sizeof(char)*5);
    memset(part, 0, 5);

    sscanf(buffer, "%d %d %d %s", &major, &minor, &blocks_size, part);
    blocks_size_mo = blocks_size / 1024;
    part = trim(part);
    printf("\t%s : %d Mo\n", part, blocks_size_mo);
    if (part) free(part);
  }
}

int main
(
  int argc,
  char ** argv
)
{
  FILE * fp;
  char * line = NULL;
  size_t length = 0;
  ssize_t read;

/* ########## cpu_info ########## */

  fp = fopen("/proc/cpuinfo", "r");

  printf("CPU : \n");

  if (fp == NULL)
  {
    exit(EXIT_FAILURE);
  }

  while ((read = getline(&line, &length, fp)) != -1)
  {
    parse_cpu(line);
  }

  printf("\n");

  fclose(fp);
  if (line) free(line);

/* ########## mem_info ########## */

  fp = fopen("/proc/meminfo", "r");

  printf("RAM : \n");

  if (fp == NULL)
  {
    exit(EXIT_FAILURE);
  }

  while ((read = getline(&line, &length, fp)) != -1)
  {
    parse_mem(line);
  }

  printf("\n");

  fclose(fp);
  if (line) free(line);

/* ########## partitions ########## */

  fp = fopen("/proc/partitions", "r");

  printf("Disk partition : \n");

  if (fp == NULL)
  {
    exit(EXIT_FAILURE);
  }

  while ((read = getline(&line, &length, fp)) != -1)
  {
    parse_disk(line);
  }

  printf("\n");

  fclose(fp);
  if (line) free(line);

/* ########## version ########## */

  fp = fopen("/proc/version", "r");

  printf("Operating system : \n");

  if (fp == NULL)
  {
    exit(EXIT_FAILURE);
  }

  while ((read = getline(&line, &length, fp)) != -1)
  {
    printf("\t%s", line);
  }

  printf("\n");

  fclose(fp);
  if (line) free(line);

  exit(EXIT_SUCCESS);
}
