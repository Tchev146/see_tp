#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <err.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sched.h>
#include <string.h>

#include <signal.h>
#include <time.h>


#define SCHED_NORMAL    0
#define SCHED_FIFO      1
#define SCHED_RR        2
#define SCHED_BATCH     3
#define SCHED_IDLE      5
#define SCHED_DEADLINE  6

void handler
(
  int sig, 
  siginfo_t *si,
  void *uc
)
{
  printf("Caught signal %d\n", sig);
}

int main
(
  int argc, 
  char ** argv
)
{
  int pid = 0;
  int sched = -1;
  int prio = -1;
  int return_value = -1;

  timer_t timerid;
  struct sigevent sev;
  struct itimerspec its;

  struct sched_param param;

  struct sigaction sa;

  sched = sched_getscheduler(pid);

  prio = getpriority(PRIO_PROCESS, 0);
  switch (sched)
  {
    case SCHED_NORMAL :
      printf("pid : %d -> SCHED_NORMAL\n");
      break;

    case SCHED_FIFO :
      printf("pid : %d -> SCHED_FIFO\n");
      break;

    case SCHED_RR :
      printf("pid : %d -> SCHED_RR\n");
      break;

    case SCHED_BATCH :
      printf("pid : %d -> SCHED_BATCH\n");
      break;

    case SCHED_IDLE :
      printf("pid : %d -> SCHED_IDLE\n");
      break;

    case SCHED_DEADLINE :
      printf("pid : %d -> SCHED_DEADLINE\n");
      break;
        
    default:
      break;
  }

  printf("Nice Value : %d\n", prio);

  setpriority(PRIO_PROCESS, 0, prio+10);
  prio = getpriority(PRIO_PROCESS, 0);
  printf("New nice value : %d\n", prio); 

  printf("set %d at SCHED_FIFO with prio %d \n", pid, prio);

  param.sched_priority = prio;

  return_value = sched_setscheduler(pid, SCHED_FIFO, &param);
  if (return_value != 0)
  {
    errx(EXIT_FAILURE, "sched_setscheduler didn't return 0");
  }

  sched = sched_getscheduler(pid);
  prio = getpriority(PRIO_PROCESS, 0);
  switch (sched)
  {
    case SCHED_NORMAL :
      printf("pid : %d -> SCHED_NORMAL\n");
      break;

    case SCHED_FIFO :
      printf("pid : %d -> SCHED_FIFO\n");
      break;

    case SCHED_RR :
      printf("pid : %d -> SCHED_RR\n");
      break;

    case SCHED_BATCH :
      printf("pid : %d -> SCHED_BATCH\n");
      break;

    case SCHED_IDLE :
      printf("pid : %d -> SCHED_IDLE\n");
      break;

    case SCHED_DEADLINE :
      printf("pid : %d -> SCHED_DEADLINE\n");
      break;
        
    default:
      break;
  }

  printf("Nice Value : %d\n", prio);

  printf("Establishing handler for signal %d\n", SIGRTMIN);
  sa.sa_flags = SA_SIGINFO;
  sa.sa_sigaction = handler;

  return_value = sigaction(SIGRTMIN, &sa, NULL);
  if (return_value != 0)
  {
    errx(EXIT_FAILURE, "sigaction didn't return 0");
  }

  sev.sigev_notify = SIGEV_SIGNAL;
  sev.sigev_signo = SIGRTMIN;
  sev.sigev_value.sival_ptr = &timerid;

  return_value = timer_create(CLOCK_REALTIME, &sev, &timerid);
  if (return_value != 0)
  {
    errx(EXIT_FAILURE, "timer_create didn't return 0\n");
  }
  
  printf("timer ID is 0x%lx\n", (long) timerid);

  /* Start the timer */
  its.it_value.tv_sec = 1;
  its.it_value.tv_nsec = 0;
  its.it_interval.tv_sec = its.it_value.tv_sec;
  its.it_interval.tv_nsec = its.it_value.tv_nsec;

  return_value = timer_settime(timerid, 0, &its, NULL);
  if (return_value != 0)
  {
    errx(EXIT_FAILURE, "timer_settime didn't return 0");
  }

  while (1)
  {
    sleep(1);
  }

  return EXIT_SUCCESS;
}