/****************************************
 * INCLUDE ******************************
 ****************************************/
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>           /* For O_* constants */
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <mqueue.h>
#include <errno.h>

/***************************************
 * DEFINE ******************************
 ***************************************/
#define NB_THREADS 4
#define VECTOR_SIZE 400

/***************************************
 * STRUCT ****************************** 
 ***************************************/
typedef struct
{
  int index_start;
  int index_end;
} thread_param;

typedef struct
{
  int vecteur1[VECTOR_SIZE];
  int vecteur2[VECTOR_SIZE];
  int resultat;
  int compteur_affichage;
} produit;

/***************************************
 * GLOBAL VARIABLE *********************
 ***************************************/

produit prod;
mqd_t fd_file;
mqd_t code;

/***************************************
 * FUNCTION ****************************
 ***************************************/

void * threadAffichage
(
  void * args
)
{
  char * res_thread = NULL;
  struct mq_attr attr;
  mq_getattr(fd_file, &attr);

  res_thread = (char *) malloc(sizeof(char)*attr.mq_msgsize);

  while(1)
  {
    code = mq_receive(fd_file, res_thread, attr.mq_msgsize, 0);
    if (code == -1)
    {
      perror("mq_receive error : ");
      pthread_exit((void *) 0);
    }

    prod.resultat += atoi(res_thread);
    prod.compteur_affichage++;
    if (prod.compteur_affichage == NB_THREADS)
      break;
  }

  if(prod.compteur_affichage == NB_THREADS)
    printf("Resultat = %d\n", prod.resultat);

  free(res_thread);
 
  pthread_exit((void *) 0);
}

void * produitScalaire
(
  void * args
)
{
  thread_param * param = (thread_param *) args;
  int i;
  int res_local = 0;
  char * buff;

  for (i = param->index_start; i < param->index_end; i++)
  {
    res_local += prod.vecteur1[i] * prod.vecteur2[i];
  }

  buff = (char *) malloc(sizeof(char)*sizeof(int));

  sprintf(buff, "%d", res_local);

  code = mq_send(fd_file, buff, sizeof(buff), 0);
  if (code == -1)
  {
    perror("mq_send error : ");
    pthread_exit((void *) 0);
  }

  free(param);

  pthread_exit((void *) 0);
}

/***************************************
 * MAIN ********************************
 ***************************************/

int main
(
  int argc,
  char ** argv
)
{
  int compteur = 0;
  int retour_code = 0;

  thread_param * param;
  
  fd_file = mq_open("/file", O_RDWR | O_CREAT, 0664, NULL);
  if (fd_file == -1)
  {
    perror("mq_open error : ");
    exit(EXIT_FAILURE);
  }

  prod.compteur_affichage = 0;
  prod.resultat = 0;

  for (compteur = 0; compteur < VECTOR_SIZE; compteur++)
  {
    prod.vecteur1[compteur] = random() % 100 + 1;
    prod.vecteur2[compteur] = random() % 100 + 1;
    //prod.vecteur1[compteur] = 1;
    //prod.vecteur2[compteur] = 1;
  }

  pthread_t thread[NB_THREADS+1];
  pthread_attr_t attr;

  void *status;

  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

  for (compteur = 0; compteur<NB_THREADS; compteur++)
  {
    printf("Creation du thread %d\n", compteur);

    param = (thread_param *) malloc(sizeof(thread_param));

    param->index_end = (VECTOR_SIZE / NB_THREADS) * (compteur + 1);
    param->index_start = param->index_end - (VECTOR_SIZE / NB_THREADS);

    retour_code = pthread_create(&thread[compteur], &attr, produitScalaire, param);
    
    if (retour_code)
    {
      printf("ERROR; le code de retour de pthread_create() est %d\n", retour_code);
      exit(EXIT_FAILURE);
    }
  }

  param = (thread_param *) malloc(sizeof(thread_param));

  param->index_end = -1;
  param->index_start = -1;

  printf("Creation du thread %d : thread affichage\n", NB_THREADS);
  retour_code = pthread_create(&thread[NB_THREADS], &attr, threadAffichage, NULL);
    
  if (retour_code)
  {
    printf("ERROR; le code de retour de pthread_create() est %d\n", retour_code);
    exit(EXIT_FAILURE);
  }

  /* liberation des attributs et attente de la terminaison des threads */
  pthread_attr_destroy(&attr);
  for(compteur=0; compteur<NB_THREADS; compteur++)
  {
    retour_code = pthread_join(thread[compteur], &status);
    if (retour_code)
    {
      printf("ERROR; le code de retour du pthread_join() est %d\n", retour_code);
      exit(EXIT_FAILURE);
    }
    printf("le join a fini avec le thread %d et a donne le status= %ld\n",compteur, (long)status);
  }

  retour_code = pthread_join(thread[NB_THREADS], &status);
  if (retour_code)
  {
    printf("ERROR; le code de retour du pthread_join() est %d\n", retour_code);
    exit(EXIT_FAILURE);
  }
  printf("le join a fini avec le thread %d et a donne le status= %ld\n",NB_THREADS, (long)status);
  
  pthread_exit(NULL);

  // Destruction de la file de messages 

  code = mq_unlink("/file");
  if (code != 0 )
  {
    printf("ERROR; le code de retour du mq_unlink() est %d\n", code);
    exit(EXIT_FAILURE);
  }

  // Ferme le descripteur de fichier
  code = mq_close(fd_file);
  if (code != 0 )
  {
    printf("ERROR; le code de retour du mq_close() est %d\n", code);
    exit(EXIT_FAILURE);
  }

  return(EXIT_SUCCESS);
}