#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>

char * trim
(
  char * buff
)
{
  //PRECEDING CHARACTERS
  while (1==1)
  {
    if ((*buff == ' ') || (*buff == '\t') || (*buff == '\r') || (*buff == '\n'))
    { 
      ++buff;
    }
    else
      break;
  }
    
  //TRAILING CHARACTERS
  int y = strlen(buff)-1;
  while (1==1)
  {
    if (buff[y] == ' ' || (buff[y] == '\t') || (buff[y] == '\r') || (buff[y] == '\n'))
    { 
      y--;
    }
    else
      break;
  }
  y = strlen(buff)-y;
  buff[strlen(buff)-y+1]='\0';
  return buff;
}

char * get_cmdline
(
  char * pid
)
{
  char path[256];
  char * cmdline = NULL;

  FILE * fp;
  char * line = NULL;
  size_t length = 0;
  ssize_t read;

  sprintf(path, "/proc/%s/cmdline", pid);
  fp = fopen(path, "r");

  if (fp == NULL)
  {
    perror(path);
    exit(EXIT_FAILURE);
  }

  while ((read = getline(&line, &length, fp)) != -1)
  {
    cmdline = (char *) malloc(sizeof(char)*strlen(line)+1);
    memset(cmdline, 0, strlen(line)+1);
    strcpy(cmdline, line);
  }

  fclose(fp);
  if (line) free(line);

  return cmdline;
}

void parse_status
( 
  char * buffer
)
{
  char delim[2] = ":";
  char * token = NULL;
  char * tok_no_space = NULL;

  int ret_strcmp = 0;

  token = strtok(buffer, delim);
  tok_no_space = trim(token);

  ret_strcmp = strcmp(tok_no_space, "Tgid");
  if (ret_strcmp == 0)
  {
    token = strtok(NULL, delim);
    tok_no_space = trim(token);
    printf("Tgid : %s\n", tok_no_space);
  }

  ret_strcmp = strcmp(tok_no_space, "PPid");
  if (ret_strcmp == 0)
  {
    token = strtok(NULL, delim);
    tok_no_space = trim(token);
    char * cmdline = get_cmdline(tok_no_space);

    printf("ppid : %s, cmdline %s\n", tok_no_space, cmdline);

    free(cmdline);
  }

  ret_strcmp = strcmp(tok_no_space, "Threads");
  if (ret_strcmp == 0)
  {
    token = strtok(NULL, delim);
    tok_no_space = trim(token);
    printf("Nb Threads : %s\n", tok_no_space);
  }

  ret_strcmp = strcmp(tok_no_space, "Uid");
  if (ret_strcmp == 0)
  {
    token = strtok(NULL, delim);
    tok_no_space = trim(token);
    printf("Uid : %s\n", tok_no_space);
  }

  ret_strcmp = strcmp(tok_no_space, "Gid");
  if (ret_strcmp == 0)
  {
    token = strtok(NULL, delim);
    tok_no_space = trim(token);
    printf("Gid : %s\n", tok_no_space);
  }

}

int main
(
  int argc,
  char ** argv
)
{

  if (argc != 2)
  {
    errx(EXIT_FAILURE, "Usage : ...\n");
  }

  char path[256];

  char * pid = NULL;
  char * cmdline = NULL;
  char * tgid = NULL;
  char * ppid = NULL;
  char * ppid_cmdline = NULL;
  char * nb_threads = NULL;
  char * uid = NULL;
  char * gid = NULL;

  FILE * fp;
  char * line = NULL;
  size_t length = 0;
  ssize_t read;


  pid = argv[1];

/* ########## cmdline ########## */

  cmdline = get_cmdline(pid);

  printf("\n");
  printf("pid processus : %s\n", pid);
  printf("cmdline : %s\n", cmdline);

/* ########## tgid, ppid, ppid cmdline, nbThreads, uid, gid ########## */

  sprintf(path, "/proc/%s/status", pid);
  fp = fopen(path, "r");

  if (fp == NULL)
  {
    perror(path);
    exit(EXIT_FAILURE);
  }

  while ((read = getline(&line, &length, fp)) != -1)
  {
    parse_status(line);
  }

  fclose(fp);
  if (line) free(line);


  printf("\n");

  free(cmdline);

  exit(EXIT_SUCCESS);
}
