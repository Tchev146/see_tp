#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

int main
(
  int argc, 
  char ** argv
)
{
  int random = 0;
  while(1)
  {
    random = rand();
    for (int i = 0; i < random; i++)
    {
      printf("i = %d, random %d \n", i, random);
      continue;
    }
    printf("sleep\n");
    sleep(random/100000);
  }
}