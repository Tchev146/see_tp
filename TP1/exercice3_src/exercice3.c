#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>
#include <unistd.h>
#include "ex3func.h"

int main
(
  int argc,
  char ** argv
)
{
  int opt = 0;
  int bIsCSet = 0, bIsMSet = 0, bIsPSet = 0, bIsVSet = 0, bIsDSet = 0;
  
  char * pid = NULL;

  if (argc <= 1)
  {
    errx(EXIT_FAILURE, "Usage :\n-c = cpu info\n-m = memory info\n-p <pid> = process <pid> info\n-v = kernel version\n-d = disk partitions\n");
  }

  while ((opt = getopt(argc, argv, "cvdmp:")) != -1)
  {
    switch (opt)
    {
      case 'c':
        bIsCSet = 1;
        break;

      case 'm':
        bIsMSet = 1;
        break;

      case 'v':
        bIsVSet = 1;
        break;

      case 'd':
        bIsDSet = 1;
        break;

      case 'p':
        bIsPSet = 1;
        pid = (char *) malloc(sizeof(char)*strlen(optarg)+1);
        memset(pid, 0, strlen(optarg)+1);
        strcpy(pid, optarg);
        break;

      default:
        errx(EXIT_FAILURE, "Usage :\n-c = cpu info\n-m = memory info\n-p <pid> = process <pid> info\n-v = kernel version\n-d = disk partitions\n");
        break;
    }
  }

  if (bIsCSet == 1)
  {
    print_cpu_info();
  }

  if (bIsMSet == 1)
  {
    print_mem_info();
  }

  if (bIsPSet == 1)
  {
    print_pid_info(pid);
  }

  if (bIsVSet == 1)
  {
    print_kernel_info();
  }

  if (bIsDSet == 1)
  {
    print_disk_info();
  }

  if (pid) free(pid);

  return EXIT_SUCCESS;
}