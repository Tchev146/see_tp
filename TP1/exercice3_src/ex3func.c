#include "ex3func.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>

void print_cpu_info
(
  void
)
{
  FILE * fp;
  char * line = NULL;
  size_t length = 0;
  ssize_t read;

  char path[256] = "/proc/cpuinfo";

  fp = fopen(path, "r");

  if (fp == NULL)
  {
    errx(EXIT_FAILURE, "%s : Can't open file", path);
  }

  printf("########## CPU INFO ##########\n");
  while ((read = getline(&line, &length, fp)) != -1)
  {
    parse_cpu_info(line);
  }
  printf("##############################\n");
  printf("\n");

  fclose(fp);
  if (line) free(line);
}

void print_mem_info
(
  void
)
{
  FILE * fp;
  char * line = NULL;
  size_t length = 0;
  ssize_t read;

  char path[256] = "/proc/meminfo";

  fp = fopen(path, "r");

  if (fp == NULL)
  {
    errx(EXIT_FAILURE, "%s : Can't open file", path);
  }

  printf("########## Memory INFO ##########\n");

  while ((read = getline(&line, &length, fp)) != -1)
  {
    parse_mem_info(line);
  }

  printf("#################################\n");

  printf("\n");

  fclose(fp);
  if (line) free(line);
}

void print_pid_info
(
  char * pid
)
{
  FILE * fp;
  char * line = NULL;
  size_t length = 0;
  ssize_t read;

  char path[256];

  char * cmdline = NULL;
  cmdline = get_pid_cmdline(pid);

  printf("########## PID INFO ##########\n");

  printf("process pid : %s\n", pid);
  printf("cmdline : %s\n", cmdline);

  sprintf(path, "/proc/%s/status", pid);
  fp = fopen(path, "r");

  if (fp == NULL)
  {
    errx(EXIT_FAILURE, "%s : Can't open file", path);
  }

  while ((read = getline(&line, &length, fp)) != -1)
  {
    parse_pid_status(line);
  }

  fclose(fp);

  printf("##############################\n");
  printf("\n");

  if (cmdline) free(cmdline);

}

void print_kernel_info
(
  void
)
{
  FILE * fp;
  char * line = NULL;
  size_t length = 0;
  ssize_t read;

  char path[256] = "/proc/version";

  fp = fopen(path, "r");

  if (fp == NULL)
  {
    errx(EXIT_FAILURE, "%s : Can't open file", path);
  }

  printf("########## Kernel INFO ##########\n");

  while ((read = getline(&line, &length, fp)) != -1)
  {
    printf("%s", line);
  }
  printf("#################################\n");

  printf("\n");

  fclose(fp);
  if (line) free(line);
}

void print_disk_info
(
  void
)
{
  FILE * fp;
  char * line = NULL;
  size_t length = 0;
  ssize_t read;

  char path[256] = "/proc/partitions";

  fp = fopen(path, "r");

  if (fp == NULL)
  {
    errx(EXIT_FAILURE, "%s : Can't open file", path);
  }

  printf("########## DISK INFO ##########\n");

  while ((read = getline(&line, &length, fp)) != -1)
  {
    parse_disk_info(line);
  }

  printf("###############################\n");
  printf("\n");

  fclose(fp);
  if (line) free(line);
}

char * trim_char
(
  char * buff
)
{
  //PRECEDING CHARACTERS
  while (1==1)
  {
    if ((*buff == ' ') || (*buff == '\t') || (*buff == '\r') || (*buff == '\n'))
    { 
      ++buff;
    }
    else
      break;
  }
    
  //TRAILING CHARACTERS
  int y = strlen(buff)-1;
  while (1==1)
  {
    if (buff[y] == ' ' || (buff[y] == '\t') || (buff[y] == '\r') || (buff[y] == '\n'))
    { 
      y--;
    }
    else
      break;
  }
  y = strlen(buff)-y;
  buff[strlen(buff)-y+1]='\0';
  return buff;
}

char * get_pid_cmdline
(
  char * pid
)
{
  char path[256];
  char * cmdline = NULL;

  FILE * fp;
  char * line = NULL;
  size_t length = 0;
  ssize_t read;

  sprintf(path, "/proc/%s/cmdline", pid);
  fp = fopen(path, "r");

  if (fp == NULL)
  {
    errx(EXIT_FAILURE, "%s : Can't open file", path);
  }

  while ((read = getline(&line, &length, fp)) != -1)
  {
    cmdline = (char *) malloc(sizeof(char)*strlen(line)+1);
    memset(cmdline, 0, strlen(line)+1);
    strcpy(cmdline, line);
  }

  fclose(fp);
  if (line) free(line);

  return cmdline;
}

void parse_pid_status
( 
  char * buffer
)
{
  char delim[2] = ":";
  char * token = NULL;
  char * tok_no_space = NULL;

  int ret_strcmp = 0;

  token = strtok(buffer, delim);
  tok_no_space = trim_char(token);

  ret_strcmp = strcmp(tok_no_space, "Tgid");
  if (ret_strcmp == 0)
  {
    token = strtok(NULL, delim);
    tok_no_space = trim_char(token);
    printf("Tgid : %s\n", tok_no_space);
  }

  ret_strcmp = strcmp(tok_no_space, "PPid");
  if (ret_strcmp == 0)
  {
    token = strtok(NULL, delim);
    tok_no_space = trim_char(token);
    char * cmdline = get_pid_cmdline(tok_no_space);

    printf("ppid : %s, cmdline %s\n", tok_no_space, cmdline);

    free(cmdline);
  }

  ret_strcmp = strcmp(tok_no_space, "Threads");
  if (ret_strcmp == 0)
  {
    token = strtok(NULL, delim);
    tok_no_space = trim_char(token);
    printf("Nb Threads : %s\n", tok_no_space);
  }

  ret_strcmp = strcmp(tok_no_space, "Uid");
  if (ret_strcmp == 0)
  {
    token = strtok(NULL, delim);
    tok_no_space = trim_char(token);
    printf("Uid : %s\n", tok_no_space);
  }

  ret_strcmp = strcmp(tok_no_space, "Gid");
  if (ret_strcmp == 0)
  {
    token = strtok(NULL, delim);
    tok_no_space = trim_char(token);
    printf("Gid : %s\n", tok_no_space);
  }
}

void parse_mem_info
(
  char * buffer
)
{
  char delim[2] = ":";
  char * token = NULL;
  char * tok_no_space = NULL;

  int ret_strcmp = 0;

  token = strtok(buffer, delim);
  tok_no_space = trim_char(token);

  ret_strcmp = strcmp(tok_no_space, "MemTotal");
  if (ret_strcmp == 0)
  {
    token = strtok(NULL, delim);
    tok_no_space = trim_char(token);
    printf("Total memory : %s\n", tok_no_space);
  }

  ret_strcmp = strcmp(tok_no_space, "MemFree");
  if (ret_strcmp == 0)
  {
    token = strtok(NULL, delim);
    tok_no_space = trim_char(token);
    printf("Free memory : %s\n", tok_no_space);
  }
}

void parse_cpu_info
(
  char * buffer
)
{
  char delim[2] = ":";
  char * token = NULL;
  char * tok_no_space = NULL;

  int ret_strcmp = 0;

  token = strtok(buffer, delim);
  tok_no_space = trim_char(token); 

  ret_strcmp = strcmp(tok_no_space, "model name");
  if (ret_strcmp == 0)
  {
    token = strtok(NULL, delim);
    tok_no_space = trim_char(token);
    printf("Model : %s\n", tok_no_space);
  }

  ret_strcmp = strcmp(tok_no_space, "cpu MHz");
  if (ret_strcmp == 0)
  {
    token = strtok(NULL, delim);
    tok_no_space = trim_char(token);
    printf("Frequency : %s\n", tok_no_space);
  }

  ret_strcmp = strcmp(tok_no_space, "cache size");
  if (ret_strcmp == 0)
  {
    token = strtok(NULL, delim);
    tok_no_space = trim_char(token);
    printf("Cache : %s\n", tok_no_space);
  }

  ret_strcmp = strcmp(tok_no_space, "address sizes");
  if (ret_strcmp == 0)
  {
    token = strtok(NULL, delim);
    tok_no_space = trim_char(token);
    printf("Espace d'adressage : %s\n\n", tok_no_space);
  }

}

void parse_disk_info
(
  char * buffer
)
{

  char * part = NULL;
  int major = 0;
  int minor = 0;
  int blocks_size = 0;
  int blocks_size_mo = 0;

  if ((strstr(buffer, "sd")) != NULL)
  {
    part = (char*) malloc(sizeof(char)*5);
    memset(part, 0, 5);

    sscanf(buffer, "%d %d %d %s", &major, &minor, &blocks_size, part);
    blocks_size_mo = blocks_size / 1024;
    part = trim_char(part);
    printf("%s : %d Mo\n", part, blocks_size_mo);
    if (part) free(part);
  }
}