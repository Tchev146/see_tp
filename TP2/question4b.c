#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <err.h>
#include <unistd.h>
#include <string.h>

#include <signal.h>
#include <time.h>

void handler
(
  int sig, 
  siginfo_t *si,
  void *uc
)
{
  printf("Caught signal %d\n", sig);
}

int main
(
  int argc, 
  char ** argv
)
{
  int return_value = -1;
  int iteration = 0;

  timer_t timerid;
  struct sigevent sev;
  struct itimerspec its;

  struct timespec ts_gettime;

  struct sigaction sa;

  printf("Establishing handler for signal %d\n", SIGRTMIN);
  sa.sa_flags = SA_SIGINFO;
  sa.sa_sigaction = handler;

  return_value = sigaction(SIGRTMIN, &sa, NULL);
  if (return_value != 0)
  {
    errx(EXIT_FAILURE, "sigaction didn't return 0");
  }

  sev.sigev_notify = SIGEV_SIGNAL;
  sev.sigev_signo = SIGRTMIN;
  sev.sigev_value.sival_ptr = &timerid;

  return_value = timer_create(CLOCK_REALTIME, &sev, &timerid);
  if (return_value != 0)
  {
    errx(EXIT_FAILURE, "timer_create didn't return 0\n");
  }
  
  printf("timer ID is 0x%lx\n", (long) timerid);

  /* Start the timer */
  its.it_value.tv_sec = 0;
  its.it_value.tv_nsec = 100000000;
  its.it_interval.tv_sec = its.it_value.tv_sec;
  its.it_interval.tv_nsec = its.it_value.tv_nsec;

  return_value = timer_settime(timerid, 0, &its, NULL);
  if (return_value != 0)
  {
    errx(EXIT_FAILURE, "timer_settime didn't return 0");
  }

  while (iteration<100)
  {
    iteration++;
    sleep(1);
    return_value = clock_gettime(CLOCK_REALTIME, &ts_gettime);
    if (return_value != 0)
    {
      errx(EXIT_FAILURE, "clock_gettime didn't return 0");
    }

    printf("clock_gettime: ts_gettime.tv_sec : %ld\n", (long) ts_gettime.tv_sec);
    printf("clock_gettime: ts_gettime.tv_nsec : %ld\n", ts_gettime.tv_nsec);

  }

  return EXIT_SUCCESS;
}