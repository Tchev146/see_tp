#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <err.h>
#include <unistd.h>
#include <string.h>

#include <signal.h>
#include <time.h>
#include <math.h>
#include <sched.h>
#include <sys/resource.h>

#define IT_MAX 100

struct timespec t_time[IT_MAX];
long iteration = 0;

void handler
(
  int sig, 
  siginfo_t *si,
  void *uc
)
{
  int return_value = -1;
  
  struct timespec ts_gettime;

  return_value = clock_gettime(CLOCK_REALTIME, &ts_gettime);
  if (return_value != 0)
  {
    errx(EXIT_FAILURE, "clock_gettime didn't return 0");
  }

  t_time[iteration] = ts_gettime;
  iteration++;
}

int main
(
  int argc, 
  char ** argv
)
{
  int return_value = -1;
  long somme_t_nsec = 0;
  long moyenne_t_nsec = 0;
  long diff_signaux[IT_MAX-1];
  long diff_nsec[IT_MAX-1];
  long ecart_type = 0;

  timer_t timerid;
  struct sigevent sev;
  struct itimerspec its;

  struct sigaction sa;
  struct sched_param param;

  int priority = 0;
  int pid = 0;

  param.sched_priority = 50;

  return_value = sched_setscheduler(pid, SCHED_FIFO, &param);
  if (return_value != 0)
  {
    perror("sched_setscheduler didn't return 0");
    exit(EXIT_FAILURE);
  }

  sa.sa_flags = SA_SIGINFO;
  sa.sa_sigaction = handler;

  return_value = sigaction(SIGRTMIN, &sa, NULL);
  if (return_value != 0)
  {
    errx(EXIT_FAILURE, "sigaction didn't return 0");
  }

  sev.sigev_notify = SIGEV_SIGNAL;
  sev.sigev_signo = SIGRTMIN;
  sev.sigev_value.sival_ptr = &timerid;

  return_value = timer_create(CLOCK_REALTIME, &sev, &timerid);
  if (return_value != 0)
  {
    errx(EXIT_FAILURE, "timer_create didn't return 0\n");
  }

  /* Start the timer */
  its.it_value.tv_sec = 0;
  its.it_value.tv_nsec = 100000000;
  its.it_interval.tv_sec = its.it_value.tv_sec;
  its.it_interval.tv_nsec = its.it_value.tv_nsec;

  return_value = timer_settime(timerid, 0, &its, NULL);
  if (return_value != 0)
  {
    errx(EXIT_FAILURE, "timer_settime didn't return 0");
  }

  while (1)
  {
    if (iteration == IT_MAX)
    {
      break;
    }
  }

  for(int i = 0; i < IT_MAX-1; i++)
  {
    time_t	  tv_sec_res  = 0;
	  long tv_nsec_res = 0;

    struct timespec t_i; 
    struct timespec t_i1;

    t_i = t_time[i];
    t_i1 = t_time[i+1];

    tv_sec_res = t_i1.tv_sec - t_i.tv_sec;
    if (t_i1.tv_nsec < t_i.tv_nsec)
    {
		  tv_sec_res--;
		  tv_nsec_res = (1000000000 - t_i.tv_nsec) + t_i1.tv_nsec;
	  }
    else
    {
      tv_nsec_res = t_i1.tv_nsec - t_i.tv_nsec;
    }

    diff_signaux[i] = tv_nsec_res;
  }

  for (int i = 0; i < IT_MAX-1; i++)
  {
    somme_t_nsec += diff_signaux[i];
  }

  moyenne_t_nsec = somme_t_nsec/(IT_MAX-1);

  for (int i = 0; i < IT_MAX-1; i++)
  {
    diff_nsec[i] = pow(diff_signaux[i] - moyenne_t_nsec, 2);
    ecart_type += diff_nsec[i];
  }

  ecart_type = sqrt(ecart_type/(IT_MAX-1));

  printf("\n");
  printf("Moyenne des temps en nsec : %ld nsec\n", moyenne_t_nsec);
  printf("Ecart type des temps en nsec : %ld nsec\n", ecart_type);

  printf("\n");

  return EXIT_SUCCESS;
}