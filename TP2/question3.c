#include <stdlib.h>
#include <stdio.h>
#include <err.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sched.h>
#include <string.h>


#define SCHED_NORMAL    0
#define SCHED_FIFO      1
#define SCHED_RR        2
#define SCHED_BATCH     3
#define SCHED_IDLE      5
#define SCHED_DEADLINE  6

int main
(
  int argc, 
  char ** argv
)
{
  int pid = 0;
  int sched = -1;
  int prio = -1;
  int return_value = -1;

  struct sched_param param;

  sched = sched_getscheduler(pid);

  prio = getpriority(PRIO_PROCESS, 0);
  switch (sched)
  {
    case SCHED_NORMAL :
      printf("pid : %d -> SCHED_NORMAL\n");
      break;

    case SCHED_FIFO :
      printf("pid : %d -> SCHED_FIFO\n");
      break;

    case SCHED_RR :
      printf("pid : %d -> SCHED_RR\n");
      break;

    case SCHED_BATCH :
      printf("pid : %d -> SCHED_BATCH\n");
      break;

    case SCHED_IDLE :
      printf("pid : %d -> SCHED_IDLE\n");
      break;

    case SCHED_DEADLINE :
      printf("pid : %d -> SCHED_DEADLINE\n");
      break;
        
    default:
      break;
  }

  printf("Nice Value : %d\n", prio);

  setpriority(PRIO_PROCESS, 0, prio-10);
  prio = getpriority(PRIO_PROCESS, 0);
  printf("New nice value : %d\n", prio); 

  printf("set %d at SCHED_FIFO with prio %d \n", pid, prio);

  param.sched_priority = prio;

  return_value = sched_setscheduler(pid, SCHED_FIFO, &param);
  if (return_value != 0)
  {
    errx(EXIT_FAILURE, "sched_setscheduler didn't return 0");
  }

  sched = sched_getscheduler(pid);
  prio = getpriority(PRIO_PROCESS, 0);
  switch (sched)
  {
    case SCHED_NORMAL :
      printf("pid : %d -> SCHED_NORMAL\n");
      break;

    case SCHED_FIFO :
      printf("pid : %d -> SCHED_FIFO\n");
      break;

    case SCHED_RR :
      printf("pid : %d -> SCHED_RR\n");
      break;

    case SCHED_BATCH :
      printf("pid : %d -> SCHED_BATCH\n");
      break;

    case SCHED_IDLE :
      printf("pid : %d -> SCHED_IDLE\n");
      break;

    case SCHED_DEADLINE :
      printf("pid : %d -> SCHED_DEADLINE\n");
      break;
        
    default:
      break;
  }

  printf("Nice Value : %d\n", prio);

  return EXIT_SUCCESS;
}