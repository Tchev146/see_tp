void print_cpu_info
(
  void
);

void print_mem_info
(
  void
);

void print_pid_info
(
  char * pid
);

void print_disk_info
(
  void
);

void print_kernel_info
(
  void
);

char * trim_char
(
  char * buff
);

char * get_pid_cmdline
(
  char * pid
);

void parse_pid_status
( 
  char * buffer
);

void parse_mem_info
(
  char * buffer
);

void parse_cpu_info
(
  char * buffer
);

void parse_disk_info
(
  char * buffer
);