#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#define VECTOR_SIZE 400

int main
(
  int argc, 
  char ** argv
)
{
  FILE * file1;
  FILE * file2;

  int buff1[VECTOR_SIZE];
  int buff2[VECTOR_SIZE];

  file1 = fopen("vec1", "w+");
  if (file1 == NULL)
  {
    perror("fopen file1 :");
    exit(EXIT_FAILURE);
  }

  for (int i  = 0; i < VECTOR_SIZE; i++)
  {
    buff1[i] = 1;
    buff2[i] = 1;
  }

  fwrite(buff1, sizeof(int), VECTOR_SIZE, file1);

  fclose(file1);

  file2 = fopen("vec2", "w+");
  if (file2 == NULL)
  {
    perror("fopen file2 :");
    exit(EXIT_FAILURE);
  }
  
  fwrite(buff2, sizeof(int), VECTOR_SIZE, file2);

  fclose(file2);

  return(EXIT_SUCCESS);
}
