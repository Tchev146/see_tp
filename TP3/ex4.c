#define _GNU_SOURCE
#include <aio.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>

void handler
(
  int sig, 
  siginfo_t *info, 
  void *ucontext
)
{
  switch(info->si_value.sival_int)
  {
    case 0: 
      printf("signal from  aio_write\n");
      break;

    case 1:
      printf("signal from aio_read\n");
      break;
  }
}

int main
(
  int argc, 
  char ** argv
)
{
  if(argc != 2)
  {
    printf("Usage: %s file\n", argv[0]);
    return (EXIT_FAILURE);
  }

  FILE * file;

  struct aiocb read1, write, read2; // bloc de contrôle de l’E/S asynchrone
  struct aiocb * cbs[3];
  struct sigaction sig;

  char * toWrite = NULL;

  __ssize_t retour_code = 0;

  toWrite = "Line wrote by aio_write";

  sigemptyset(&sig.sa_mask);
	sig.sa_flags	= SA_SIGINFO | SA_RESTART;
	sig.sa_sigaction = handler;

	retour_code = sigaction(SIGRTMIN, &sig, NULL);
	if (retour_code == -1)
  {
    perror("sigaction : ");
    exit(EXIT_FAILURE);
  }

  //Ouverture du fichier (spécifié en argument) sur lequel l’E/S va être effectuée
  file = fopen(argv[1], "r+");
  if (file == NULL)
  {
    perror("fopen");
    exit(EXIT_FAILURE);
  }

  //definition du bloc de contrôle de l’entrée/sortie read1
  read1.aio_buf = malloc(256);
  read1.aio_fildes = fileno(file); //récupérer le descripteur d’un fichier à partir de son nom
  read1.aio_nbytes = 255;
  read1.aio_offset = 0;
  read1.aio_reqprio = 0;
  read1.aio_sigevent.sigev_notify = SIGEV_SIGNAL;
	read1.aio_sigevent.sigev_signo  = SIGRTMIN;
  read1.aio_sigevent.sigev_value.sival_int = 1;

  //definition du bloc de contrôle de l’entrée/sortie read2
  read2.aio_buf = malloc(256);
  read2.aio_fildes = fileno(file); //récupérer le descripteur d’un fichier à partir de son nom
  read2.aio_nbytes = 255;
  read2.aio_offset = 0;
  read2.aio_reqprio = 0;
  read2.aio_sigevent.sigev_notify = SIGEV_SIGNAL;
	read2.aio_sigevent.sigev_signo  = SIGRTMIN;
  read2.aio_sigevent.sigev_value.sival_int = 1;

  //definition du bloc de contrôle de l’entrée/sortie read2
  write.aio_fildes = fileno(file); //récupérer le descripteur d’un fichier à partir de son nom
  write.aio_nbytes = strlen(toWrite);
  write.aio_offset = 0;
  write.aio_reqprio = 0;
  write.aio_buf = malloc(strlen(toWrite));
  memcpy((char *)write.aio_buf, toWrite, strlen(toWrite));
  write.aio_sigevent.sigev_notify = SIGEV_SIGNAL;
	write.aio_sigevent.sigev_signo  = SIGRTMIN;
  write.aio_sigevent.sigev_value.sival_int = 0;

  //lancer la lecture
  retour_code = aio_read(&read1);
  if (retour_code == -1)
  {
    perror("aio_read(&read1) : ");
    exit(EXIT_FAILURE);
  }

  retour_code = aio_write(&write);
  if (retour_code == -1)
  {
    perror("aio_write(&write) : ");
    exit(EXIT_FAILURE);
  }

  retour_code = aio_read(&read2);
  if (retour_code == -1)
  {
    perror("aio_read(&read2) : ");
    exit(EXIT_FAILURE);
  }

  //Suspension du processus dans l’attente de la terminaison de la lecture
  cbs[0] = &read1;
  cbs[1] = &write;
  cbs[2] = &read2;

  int request = 3;

	while (request > 0)
  {
		for (int i = 0; i < 3; i++)
    {
			if (cbs[i] != NULL)
      {
        retour_code = aio_error(cbs[i]);
				if (retour_code == 0)
        {
					cbs[i] = NULL;
					request--;
				}
			}
		}
	}

  retour_code = aio_return(&read1);
  if (retour_code == -1)
  {
    perror("aio_return read1 : ");
    exit(EXIT_FAILURE);
  }
  printf("operation AIO read1 a retouree %d\n", retour_code);
  printf("read1.aio_buf : %s\n", read1.aio_buf);

  retour_code = aio_return(&write);
  if (retour_code == -1)
  {
    perror("aio_return write : ");
    exit(EXIT_FAILURE);
  }
  printf("operation AIO write a retouree %d\n", retour_code);

  retour_code = aio_return(&read2);
  if (retour_code == -1)
  {
    perror("aio_return read2 : ");
    exit(EXIT_FAILURE);
  }
  printf("operation AIO read2 a retouree %d\n", retour_code);
  printf("read2.aio_buf : %s\n", read2.aio_buf);

  free((void *) read1.aio_buf);
  free((void *) read2.aio_buf);

  fclose(file);

  return(EXIT_SUCCESS);
}