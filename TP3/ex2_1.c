/****************************************
 * INCLUDE ******************************
 ****************************************/
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>           /* For O_* constants */
#include <unistd.h>
#include <sys/types.h>
#include <string.h>


/***************************************
 * DEFINE ******************************
 ***************************************/
#define NB_THREADS 4
#define VECTOR_SIZE 400

/***************************************
 * STRUCT ****************************** 
 ***************************************/
typedef struct
{
  int index_start;
  int index_end;
} thread_param;

typedef struct
{
  int vecteur1[VECTOR_SIZE];
  int vecteur2[VECTOR_SIZE];
  int resultat;
} produit;

/***************************************
 * GLOBAL VARIABLE *********************
 ***************************************/
pthread_mutex_t verrou_res;

produit prod;

/***************************************
 * FUNCTION ****************************
 ***************************************/

void * produitScalaire
(
  void * args
)
{
  thread_param * param = (thread_param *) args;
  int i;
  int res_local = 0;

  for (i = param->index_start; i < param->index_end; i++)
  {
    res_local += prod.vecteur1[i] * prod.vecteur2[i];
  }

  pthread_mutex_lock(&verrou_res);
  prod.resultat += res_local;
  pthread_mutex_unlock(&verrou_res);

  free(param);

  pthread_exit((void *) 0);
}

/***************************************
 * MAIN ********************************
 ***************************************/

int main
(
  int argc,
  char ** argv
)
{
  int compteur = 0;
  int retour_code = 0;
  int file_desc = 0;
  void * memoire;

  thread_param * param;

  prod.resultat = 0;

  for (compteur = 0; compteur < VECTOR_SIZE; compteur++)
  {
    prod.vecteur1[compteur] = random() % 100 + 1;
    prod.vecteur2[compteur] = random() % 100 + 1;
    //prod.vecteur1[compteur] = 1;
    //prod.vecteur2[compteur] = 1;
  }

  pthread_t thread[NB_THREADS];
  pthread_attr_t attr;

  void *status;

  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

  for (compteur = 0; compteur<NB_THREADS; compteur++)
  {
    printf("Creation du thread %d\n", compteur);

    param = (thread_param *) malloc(sizeof(thread_param));

    param->index_end = (VECTOR_SIZE / NB_THREADS) * (compteur + 1);
    param->index_start = param->index_end - (VECTOR_SIZE / NB_THREADS);

    retour_code = pthread_create(&thread[compteur], &attr, produitScalaire, param);
    
    if (retour_code)
    {
      printf("ERROR; le code de retour de pthread_create() est %d\n", retour_code);
      exit(EXIT_FAILURE);
    }
    
  }

  /* liberation des attributs et attente de la terminaison des threads */
  pthread_attr_destroy(&attr);
  for(compteur=0; compteur<NB_THREADS; compteur++)
  {
    retour_code = pthread_join(thread[compteur], &status);
    if (retour_code)
    {
      printf("ERROR; le code de retour du pthread_join() est %d\n", retour_code);
      exit(EXIT_FAILURE);
    }
    printf("le join a fini avec le thread %d et a donne le status= %ld\n",compteur, (long)status);
  }
  
  printf("Resultat = %d\n", prod.resultat);

  pthread_mutex_destroy(&verrou_res);
  pthread_exit(NULL);

  return(EXIT_SUCCESS);
}