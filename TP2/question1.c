#include <stdlib.h>
#include <stdio.h>
#include <err.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sched.h>
#include <string.h>


#define SCHED_NORMAL    0
#define SCHED_FIFO      1
#define SCHED_RR        2
#define SCHED_BATCH     3
#define SCHED_IDLE      5
#define SCHED_DEADLINE  6

char * trim_char
(
  char * buff
)
{
  //PRECEDING CHARACTERS
  while (1==1)
  {
    if ((*buff == ' ') || (*buff == '\t') || (*buff == '\r') || (*buff == '\n'))
    { 
      ++buff;
    }
    else
      break;
  }
    
  //TRAILING CHARACTERS
  int y = strlen(buff)-1;
  while (1==1)
  {
    if (buff[y] == ' ' || (buff[y] == '\t') || (buff[y] == '\r') || (buff[y] == '\n'))
    { 
      y--;
    }
    else
      break;
  }
  y = strlen(buff)-y;
  buff[strlen(buff)-y+1]='\0';
  return buff;
}

void parse_self_sched
(
  char * buffer
)
{
  char delim[2] = ":";
  char * token = NULL;
  char * tok_no_space = NULL;

  int ret_strcmp = 0;

  token = strtok(buffer, delim);
  tok_no_space = trim_char(token);

  ret_strcmp = strcmp(tok_no_space, "prio");
  if (ret_strcmp == 0)
  {
    token = strtok(NULL, delim);
    tok_no_space = trim_char(token);
    printf("Priority : %s\n", tok_no_space);
  }
}

void print_self_sched
(
  void
)
{
  FILE * fp;
  char * line = NULL;
  size_t length = 0;
  ssize_t read;

  char path[256] = "/proc/self/sched";

  fp = fopen(path, "r");

  if (fp == NULL)
  {
    errx(EXIT_FAILURE, "%s : Can't open file", path);
  }

  while ((read = getline(&line, &length, fp)) != -1)
  {
    parse_self_sched(line);
  }

  fclose(fp);
  if (line) free(line);
}

int main
(
  int argc, 
  char ** argv
)
{
  int opt = 0;
  int pid = -1;
  int sched = -1;
  int prio = -1;

  if (argc <= 1)
  {
    errx(EXIT_FAILURE, "Usage :\n...\n");
  }

  pid = getpid();

  while ((opt = getopt(argc, argv, "sp")) != -1)
  {
    switch (opt)
    {
      case 's':
        sched = sched_getscheduler(pid);
        prio = getpriority(PRIO_PROCESS, 0);
        switch (sched)
        {
          case SCHED_NORMAL :
            printf("pid : %d -> SCHED_NORMAL\n");
            break;

          case SCHED_FIFO :
            printf("pid : %d -> SCHED_FIFO\n");
            break;

          case SCHED_RR :
            printf("pid : %d -> SCHED_RR\n");
            break;

          case SCHED_BATCH :
            printf("pid : %d -> SCHED_BATCH\n");
            break;

          case SCHED_IDLE :
            printf("pid : %d -> SCHED_IDLE\n");
            break;

          case SCHED_DEADLINE :
            printf("pid : %d -> SCHED_DEADLINE\n");
            break;
        
          default:
              break;
        }
        printf("Nice Value : %d\n", prio);
        break;

      case 'p':
        print_self_sched();
        break;

      default:
        errx(EXIT_FAILURE, "Usage :\n...\n");
        break;
    }
  }

  return EXIT_SUCCESS;
}